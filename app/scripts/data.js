init.data = {
    share: {
        moment: {
            title: '申领福利|快来体验植村秀圣诞巧克力妆容',
            imgUrl: init.site.getPath() + 'images/share_img.jpg',
            link: init.site.url
        },
        friend: {
            title: '申领福利|快来体验植村秀圣诞巧克力妆容',
            desc: '#巧克力色诱#，你敢“尝”鲜吗？',
            imgUrl: init.site.getPath() + 'images/share_img.jpg',
            link: init.site.url
        }
    }
};