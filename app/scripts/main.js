//fix: 在ios微信下双击右侧边缘会上移
(function() {
    var agent = navigator.userAgent.toLowerCase(); //检测是否是ios
    var iLastTouch = null; //缓存上一次tap的时间
    if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
        document.body.addEventListener('touchend', function(event) {
            var iNow = new Date().getTime();
            iLastTouch = iLastTouch || iNow + 1 /** 第一次时将iLastTouch设为当前时间+1 */ ;
            var delta = iNow - iLastTouch;
            if (delta < 500 && delta > 0) {
                event.preventDefault();
                return false;
            }
            iLastTouch = iNow;
        }, false);
    }
})();

function getQueryParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
var openid = getQueryParameter('openid');
var weibo = getQueryParameter('utm_source');

var app;
$(document).ready(function() {
    function Index() {
        this.init();
        // console.log($('.page1 .ctxBox').offset().top)
        // if ($('.page1 .ctxBox').offset().top <= 0) {
        //     $('.page1 .ctxBox').css({ 'top': '1%', 'transform': 'translate3d(-50%, 0, 0)' })
        // } else {
        //     $('.page1 .ctxBox').css({ 'top': '42%', 'transform': 'translate3d(-50%, -50%, 0)' })
        // }
        // if ($('.page2 .ctxBox').offset().top <= 0) {
        //     $('.page2 .ctxBox').css({ 'top': '1%', 'transform': 'translate3d(-50%, 0, 0)' })
        // } else {
        //     $('.page2 .ctxBox').css({ 'top': '42%', 'transform': 'translate3d(-50%, -50%, 0)' })
        // }
    }
    Index.prototype = {
        constructor: Index,
        oauth: function() {
            var self = this;
            $.ajax({
                url: domain + 'interface/WeChatService.ashx?action=checklogin',
                type: 'get',
                dataType: 'json',
                success: function(msg) {
                    if (msg.Result) {
                        msg.Obj = JSON.parse(msg.Obj);
                        self.userInfo = msg;
                        if (!self.userInfo.Obj.openid) {
                            self.userInfo = {
                                Obj: {
                                    'openid': -1
                                }
                            }
                        }
                        self.bind();
                    } else {
                        location.href = domain + 'interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(init.site.url);

                    }
                }
            })
        },
        init: function() {

            this.currentPage = 1;
            this.userInfo = {
                Obj: {
                    'openid': 0
                }
            }
            if (!openid) {}
            // if (browser.versions.weixin) {
            //     this.oauth();
            // } else {
            this.bind();
            // }


            this.time = 60;
            this.maxTime = 60;
            this.tTime;

            this.canReceive = true;

        },
        checkName: function(name) {
            var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
            if (name == '') {
                alert('请输入姓名')
                return;
            }
            if (!reg.test(name)) {
                alert('名字格式不正确')
                return false;
            } else {
                return true;
            }
        },
        checkNum: function(phoneNum) {
            if (phoneNum == '') {
                alert('请输入手机号');
                return;
            }
            if (!/^1[0-9][0-9]\d{8}$/.test(phoneNum)) {
                alert('请输入正确的电话号码');
                return false;
            } else {
                return true;
            }
        },
        checkCode: function(codeVal) {
            if (codeVal == '') {
                alert('请输入验证码');
                return false;
            } else {
                return true;
            }
        },
        addAllow: function(ruleNameClass) {
            if (ruleNameClass.hasClass('active')) {
                ruleNameClass.removeClass('active');
            } else {
                ruleNameClass.addClass('active');
            }
        },
        isAllowRule: function(ruleNameClass) {
            var a = $('.' + ruleNameClass);
            var rule = a.attr('data-rule');
            if ($('.' + ruleNameClass).hasClass('active')) {
                return true
            } else {
                alert('请同意' + rule);
                return false;
            }
        },
        timeCountDown: function(t, func) {
            var self = this;
            //倒计时
            function changeTime(t) {
                if (self.tTime == null) {
                    self.tTime = new Date().getTime();
                } else {
                    var t2 = new Date().getTime();
                    if (t2 - self.tTime < self.maxTime * 1000) {
                        self.tTime = t2;
                        return;
                    } else { self.tTime = t2; }
                }

                func();
                changeTimeNext(t);
            }

            function changeTimeNext(t) {
                if (self.time == 0) {
                    t.attr('disabled', false);
                    t.html('发送验证码');
                    self.time = self.maxTime;
                    clearTimeout(st);
                } else {
                    t.attr('disabled', true);
                    t.html(self.time + ' 秒');
                    self.time--;
                    var st = setTimeout(function() {
                        changeTimeNext(t);
                    }, 1000);
                }
            }
            changeTime(t);
        },
        //店铺下拉列表
        getshoppes: function() {
            $.ajax({
                url: domain + 'interface/SampleApplyService.ashx?action=getshoppes',
                type: 'get',
                dataType: 'json',
                success: function(msg) {
                    init.shops = msg;
                    // if (!init.ba) {
                    for (var i = 0; i < init.shops.length; i++) {
                        $('.city_sec').append('<option value=\'' + init.shops[i].city + '\'>' + init.shops[i].city + '</option>');
                    }
                },
                error: function(msg) {
                    init.mySwiper.slideTo(1, 0, false);
                    alert('柜台信息获取失败')
                }
            })
        },
        postPhone: function(phoneNum) {
            var self = this;
            var a = new Date().getTime();
            // console.log()
            $.ajax({
                url: domain1 + 'apply/SendCode',
                type: 'post',
                dataType: 'json',
                data: {
                    mobile: phoneNum,
                    timestamp: a,
                    sign: hex_md5(phoneNum + a),
                },
                success: function(msg) {
                    if (msg.isSuccess && msg.Code == '0000') {
                        console.log('code-success');
                    } else {
                        alert(msg.Message);
                    }
                },
                error: function(msg) {
                    alert(msg)
                }
            })
        },
        validateCode: function() {
            var self = this;

            $.ajax({
                url: domain1 + 'apply/Apply',
                type: 'post',
                dataType: 'json',
                data: {
                    name: self.name,
                    openid: browser.versions.weixin ? self.userInfo.Obj.openid : '',
                    mobile: self.phoneNum,
                    code: self.mathVal,
                    city: $('.city_sec').val(),
                    store: $('.store_sec').val(),
                    utmSource: weibo ? weibo : '',
                },
                success: function(msg) {

                    if (msg.isSuccess && msg.Code == '0000') {

                        if (browser.versions.weixin) {
                            $('.warp').addClass('active');
                            $('.shareImg').show();
                        }

                        $('.page3').show();
                        $('.page2').hide();
                    } else if (!msg.isSuccess && msg.Code == '-1') {
                        alert('申领失败')
                    } else if (!msg.isSuccess && msg.Message == '已申领'){
                        alert('尊敬的顾客，您本月已体验过植村秀小样申领，请下月再点击体验。感谢您的支持!')
                    } else {
                        alert(msg.Message);
                    }
                },
                error: function(msg) {
                    alert('error 002')
                }
            })
        },
        trackga: function(n) {
            gaTracker('Truemetrics.send', 'event', 'SHU sampling', 'MO', n);
        },
        sendFl: function(u5) {
            var self = this;
            self.sendPv(u5);
            self.sendUv(u5);
        },
        sendPv: function(u5) {
            var axel = Math.random() + '';
            var a = axel * 10000000000000;
            var spotpix = new Image();
            spotpix.src = 'http://4233034.fls.doubleclick.net/activityi;src=4233034;type=2018o0;cat=2018o0;u2=shuuemura;u3=http://campaign.shuuemura.com.cn/FOUNDATIONSAMPLING2018;u4=MB;u5=' + u5 + ';ord=' + a;
        },
        sendUv: function(u5) {
            var axel = Math.random() + '';
            var a = axel * 10000000000000;
            var spotpix = new Image();
            spotpix.src = 'http://4233034.fls.doubleclick.net/activityi;src=4233034;type=2018o0;cat=2018o00;u2=shuuemura;u3=http://campaign.shuuemura.com.cn/FOUNDATIONSAMPLING2018;u4=MB;u5=' + u5 + ';ord=1;num=1';
        },
        bind: function() {
            var self = this;

            self.getshoppes();

            //切换城市-并居中
            $('.city_sec').change(function() {
                init.shopInfoCity = $(this).val();

                // if (init.shopInfoCity.length == 2) {
                // $(this).css('paddingLeft', '42%');
                // }
                if (init.shopInfoCity != '城市') {
                    $('.store_sec').empty();
                    $('.store_sec').append('<option value=\'专柜\'>专柜</option>');
                    for (var i = 0; i < init.shops.length; i++) {
                        if (init.shops[i].city == init.shopInfoCity) {
                            for (var j = 0; j < init.shops[i].shop.length; j++) {
                                $('.store_sec').append('<option value=\'' + init.shops[i].shop[j].name + '\'>' + init.shops[i].shop[j].name + '</option>');
                            }
                        }
                    }
                }
            })














            //验证码
            $('.codeImg').on('click', function() {
                var phoneNum = $('.phoneVal').val();

                if (self.checkNum(phoneNum)) {

                    self.timeCountDown($(this), function() { self.postPhone(phoneNum) });

                }
            })




            $('.rule').on('click', function() {
                self.addAllow($(this));
            })
            $('.rule i').on('click', function() {
                $('.ruleBox').fadeIn();
            })

            $('.privacyBox').on('click', function() {
                self.addAllow($(this));
            })
            $('.privacyBox i').on('click', function() {
                window.location.href = 'http://policy.lorealchina.com/privacypolicy';
            })

            // 页面切换
            $('.applyBtn').on('click', function() {
                if (self.isAllowRule('rule')) {

                    $('.page2').show();
                    $('.page1').hide();

                    self.trackga('SHU_2018 Xmas_MO_1_GoSampling-1');
                    self.sendFl('SHU_2018 Xmas_MO_1_GoSampling-1');
                }
            })
            $('.ruleBox').on('click', function() {
                $(this).fadeOut();
            })

            //申领
            $('.applyBtn2').on('click', function() {
                self.name = $('.nameVal').val();
                self.phoneNum = $('.phoneVal').val();
                self.mathVal = $('.codeVal').val();

                if (self.checkName(self.name) && self.checkNum(self.phoneNum) && self.checkCode(self.mathVal) && self.isAllowRule('privacyBox')) {

                    self.validateCode();

                    self.trackga('SHU_2018 Xmas_MO_2_LeadsSubmit-1');
                    self.sendFl('SHU_2018 Xmas_MO_2_LeadsSubmit-1');
                }

                // if (browser.versions.weixin) {
                //     $('.shareImg').show();
                // }

                // $('.page3').show();
                // $('.page2').hide();
            })
            $('.backBtn').on('click', function() {
                $('.page1').show();
                $('.page2').hide();

                self.trackga('SHU_2018 Xmas_MO_3_GoBack-1');
                self.sendFl('SHU_2018 Xmas_MO_3_GoBack-1');

            })
            $('.searchBtn').on('click', function() {
                self.trackga('SHU_2018 Xmas_MO_4_GoStoreDetail-1');
                self.sendFl('SHU_2018 Xmas_MO_4_GoStoreDetail-1');

                if (browser.versions.weixin) {
                    // window.location.href = 'http://wechat.shuuemura.com.cn/STORELOCATOR2018/';
                    window.location.href = 'http://www.shuuemura.com.cn/on/demandware.store/Sites-shubeauty-cn-Site/zh_CN/Stores-Show';
                } else {
                    window.location.href = 'http://www.shuuemura.com.cn/on/demandware.store/Sites-shubeauty-cn-Site/zh_CN/Stores-Show';
                }
            })

            //加载页面
            self.sendFl('SHU_2018 Xmas_MO_Landing Page');
        },
    };
    app = new Index();
});