(function(){

  //正式环境获取微信接口
  // if(!init.isTest){
    $.ajax({
      url: domain + 'interface/WeChatService.ashx?action=jssdk',
      type: 'get',
      data:{url:encodeURIComponent(init.site.url)},
      dataType:'json',
      success:function(config){
        console.log(config);
        var wxConfig={
          debug: false,
          appId: config.AppId,
          timestamp:config.Timestamp,
          nonceStr: config.NonceStr,
          signature: config.Signature,
          jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ','onMenuShareWeibo']
        };
        wx.config(wxConfig);
      }
    });

    //绑定微信分享事件
    try{
      wx.ready(function(){

        //设置分享后的回调函数
        var callback={
          success:function (msg){
            msg=JSON.stringify(msg).toLowerCase();
            if(msg.indexOf('timeline')>-1){
              //分享到朋友圈
              trackEvent('share_moment')
            }else if(msg.indexOf('message')>-1){
              //分享到给朋友
              trackEvent('share_friend')
            }
          }
        };
        wx.onMenuShareTimeline($.extend({},init.data.share.moment,callback));
        wx.onMenuShareAppMessage($.extend({},init.data.share.friend,callback));
      });

    }catch(e){
      var msg='错误：微信js-sdk未引用或者错误!';
      try{
        console.log(msg);
      }catch(e){
        alert(msg);
      }
      return;
    }
  // }


})();
