/**
 * 页面第一个加载的JS，发布后会内嵌在HTML中
 * 检查、设置网站状态,设置基础字体，跳转link 等。
 * isTest: 是否在测试服务器
 * init.psd.p(): PSD的源文件的高宽比
 */
var init = {
    site: {
        url: window.location.href.split('#')[0],
        getPath: function() {
            return this.url.substring(0, this.url.lastIndexOf('/') + 1)
        }
    },
    psd: {
        w: 750, // PSD 宽
        h: 1206, // PSD 高
        p: function() { //高宽比
            return this.h / this.w;
        }
    },
    bNumber: 0,
    bN: 0,
    userInfo: {},
};

//获取列表
var domain = 'http://wechat.shuuemura.com.cn/api/';
var domain1 = 'http://campaign.shuuemura.com.cn/FOUNDATIONSAMPLING2018api/';
// 浏览器终端判断
var browser = {
    versions: function() {
        var u = navigator.userAgent,
            app = navigator.appVersion;
        return {
            weixin: u.indexOf('MicroMessenger') > -1, //是否微信
            weibo: u.indexOf('weibo') > -1, //是否微博
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
}