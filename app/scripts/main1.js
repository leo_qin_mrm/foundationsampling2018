//fix: 在ios微信下双击右侧边缘会上移
(function () {
    var agent = navigator.userAgent.toLowerCase(); //检测是否是ios
    var iLastTouch = null; //缓存上一次tap的时间
    if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
        document.body.addEventListener('touchend', function (event) {
            var iNow = new Date().getTime();
            iLastTouch = iLastTouch || iNow + 1 /** 第一次时将iLastTouch设为当前时间+1 */;
            var delta = iNow - iLastTouch;
            if (delta < 500 && delta > 0) {
                event.preventDefault();
                return false;
            }
            iLastTouch = iNow;
        }, false);
    }
})();
$(document).ready(function(){
    init.openid = getQueryParameter('openid');

    Pace.on('hide', function(){
        setTimeout(function(){
            $('.loading').fadeOut();
        },50);
    });
    //授权流程
    init.getOauth={
    	oauth:function() {
            $.ajax({
                url: domainMYD + 'interface/WeChatService.ashx?action=checklogin',
                type: 'get',
                dataType: 'json',
                success: function (msg) {
                    if(msg.Result){
                        msg.Obj=JSON.parse(msg.Obj);
                        init.userInfo=msg;
                        init.getOauth.checkMember();
                    }else{
                        location.href = domainMYD + 'interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(init.site.url);
                    }
                }
            });
        },
        //会员
        checkMember:function() {
            $.ajax({
                url: _domainMYD + 'interface/MemberService.ashx?action=checkmembersiebel',
                type: 'post',
                dataType: 'json',
                data:{
                    tel:init.userInfo.Mobile,
                    openid:init.userInfo.Obj.openid
                },
                success: function (msg) {
                    
                    if(!msg.Result){
                        //不是会员
                        if(msg.Code==1015){
                            $('.warp').hide();
                            alert('仅限会员参加');
                        //会员提交过调查
                        }else if(msg.Code==1019){
                            $('.warp').hide();
                            var r=confirm('您已提交过满意度调查');
                            if(r){
                                WeixinJSBridge.call('closeWindow');
                            }else{
                                WeixinJSBridge.call('closeWindow');
                            }
                        }else if(msg.Code==1002){
                            alert('服务器繁忙，请稍后再试')
                        }
                    }else{
                        //是会员 code=1016
                        init.getOauth.main();
                        $('.warp').css('opacity',1);
                    }
                }
            });
        },
        // 提交
        postData:function() {
            $.ajax({
                url: domainMYD + 'interface/QuestionService.ashx?action=save',
                type: 'post',
                dataType: 'json',
                data:{
                    // openid:init.userInfo.Obj.openid,
                    openid:'0000',
                    question1:init.postData,
                    question2:encodeURIComponent($('#txtarea').val())
                },
                success: function (msg) {
                    if(msg.Result){
                        // init.postResult=true;
                        // alert('感谢您的评价');
                        // var r=confirm('感谢您的评价');
                        var r=confirm('您已提交成功');
                        if(r){
                            // WeixinJSBridge.call('closeWindow');
                            // $('body').css('display','none');
                            window.location.href='http://www.shuuemura.com.cn';
                        }else{
                            // WeixinJSBridge.call('closeWindow');
                            // $('body').css('display','none');
                            window.location.href='http://www.shuuemura.com.cn';
                        }
                    }
                }
            });
        },
        main:function() {
        	$('.ot1Box .ot').on('click',function(){
        		init.data = $(this).attr('attr-data');
        		$(this).addClass('active').siblings().removeClass('active');
        	})
        	$('#txtarea').on('input propertychange',function(){
				var length = $(this).val().length;
				$('.ot2Txt .tips span').html(length);
			});
            $('.btn').on('click',function(){

                if(!waitTime()){return;}

                if(init.postResult){
                  WeixinJSBridge.call('closeWindow');
                  return;
                }
                if(!init.data){
                    alert('您还未选择满意度');
                }else{
                    if(init.data=='ot1'){
                        init.postData='a';
                    }else if(init.data=='ot2'){
                        init.postData='b';
                    }else if(init.data=='ot3'){
                        init.postData='c';
                    }else if(init.data=='ot4'){
                        init.postData='d';
                    }
                    init.getOauth.postData();
                }
                
            })
        },
    };

    // init.getOauth.oauth();
    init.getOauth.main();
    $('.warp').css('opacity',1);

});

//fix: 在ios微信下双击右侧边缘会上移
(function () {
    var agent = navigator.userAgent.toLowerCase(); //检测是否是ios
    var iLastTouch = null; //缓存上一次tap的时间
    if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
        document.body.addEventListener('touchend', function (event) {
            var iNow = new Date().getTime();
            iLastTouch = iLastTouch || iNow + 1 /** 第一次时将iLastTouch设为当前时间+1 */;
            var delta = iNow - iLastTouch;
            if (delta < 500 && delta > 0) {
                event.preventDefault();
                return false;
            }
            iLastTouch = iNow;
        }, false);
    }
})();
function getQueryParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
      results = regex.exec(location.search);
  return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

//防止连续点击
var tTime;
function waitTime() {
  if (tTime == null){tTime = new Date().getTime();return true;
  }else{
    var t2 = new Date().getTime();
    if(t2 - tTime < 1500){tTime = t2;return false;
    }else{tTime = t2;return true;}
  }
}
//tracking
function trackEvent(name) {
    gtag('event', name, {
      'event_category': '',
      'event_label': 'click'
    });
}

