$(document).ready(function(){
    init.openid = getQueryParameter('openid');
    //授权流程
    init.getOauth={
        oauth:function() {
            $.ajax({
                url: domain + 'interface/WeChatService.ashx?action=checklogin',
                type: 'get',
                dataType: 'json',
                success: function (msg) {
                    if(msg.Result){
                        msg.Obj=JSON.parse(msg.Obj);
                        init.userInfo=msg;
                        // init.getOauth.checkmemberbind();
                        init.getOauth.getapply();
                    }else{
                        location.href = domain + 'interface/WeChatService.ashx?action=oauth&backurl=' + encodeURIComponent(init.site.url);
                        // init.getOauth.getSampleQuantity();
                        //     $('.shu').css('opacity',1);
                    }
                }
            })
        },
        //判断绑定
        checkmemberbind:function(){
            $.ajax({
                url: domain+'interface/MemberService.ashx?action=checkmemberbind',
                type: 'post',
                dataType: 'json',
                data:{
                    openid:init.userInfo.Obj.openid,
                    headimg:init.userInfo.Obj.headimgurl
                },
                success: function (msg) {
                    if(msg.Result){
                        // init.getOauth.getapply();
                        $('.fcBoxBg2').hide();
                        init.getOauth.getshoppes();
                    }else{
                        window.location.href= linkBinding;
                    }
                }
            })
        },
        //获取申领记录
        getapply:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=getapply',
                type: 'post',
                dataType: 'json',
                data:{
                    openid:init.userInfo.Obj.openid,
                },
                success: function (msg) {
                    if(msg.Result){
                        //已经申领
                        if(msg.Obj){
                            msg.Obj=JSON.parse(msg.Obj);
                            init.ba=true;
                            //
                            init.userInfo.mobile=msg.Obj.userPhone;
                            init.userInfo.getSampleId=msg.Obj.Id;
                            if(msg.Obj.Status==0){
                                //未到柜台领取BA
                                init.storeId=msg.Obj.shoppename;
                                init.getOauth.getshoppes();
                            }else{
                                //已经领取
                                init.mySwiper.slideTo(5,0,false);
                                $('.shu').css('opacity',1);
                            }
                        }else{
                            //未申领
                            init.getOauth.getSampleQuantity();
                            $('.shu').css('opacity',1);
                        }
                    }else{
                        if(msg.Code=='2009'){
                            window.location.href= linkBinding;
                        }else{
                            alert('false'+msg.Code)
                        }
                        
                    }

                }
            })
        },
        //获取小样列表
        getSampleQuantity:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=getlist',
                type: 'get',
                dataType: 'json',
                success: function (msg) {
                    init.sampleQuantity=msg;
                    appendSample();
                }
            })
        },
        //店铺下拉列表
        getshoppes:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=getshoppes',
                type: 'get',
                dataType: 'json',
                success: function (msg) {
                    init.shops=msg;
                    if(!init.ba){
                        for (var i = 0; i < init.shops.length; i++) {
                            $('.city_sec').append('<option value=\'' + init.shops[i].city + '\'>' + init.shops[i].city + '</option>');
                        }
                        init.mySwiper.slideTo(1,0,false);
                    }else{
                        for (var i = 0; i < init.shops.length; i++) {
                            for(var j=0;j< init.shops[i].shop.length;j++){
                                if(init.shops[i].shop[j].id==init.storeId){
                                    init.storeName=init.shops[i].shop[j].name;
                                }
                            }
                        }
                        $('.slide3 .txt').find('span').html(init.storeName)
                        init.mySwiper.slideTo(3,0,false);
                        $('.shu').css('opacity',1);
                    }
                },
                error:function(msg){
                    init.mySwiper.slideTo(1,0,false);
                    alert('柜台信息获取失败')
                }
            })
        },
        //申领小样
        sampleApply:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=apply',
                type: 'post',
                dataType: 'json',
                data:{
                    openid:init.userInfo.Obj.openid,
                    shoppecode:init.userInfo.shopId,
                    username:init.userInfo.Obj.nickname,
                    usertel:init.userInfo.Obj.usertel,
                    samplyId:init.userInfo.sampleId
                },
                success: function (msg) {
                    if(msg.Result){
                        if(msg.Code=='1000'){
                            $('#getName').find('span').html(init.userInfo.Obj.nickname);
                            init.mySwiper.slideTo(2,0,false);
                        }else if(msg.Code=='1007'){
                            alert('本活动每个手机号仅可参与一次。请待下月，继续申领哦。')
                        }
                    }else{
                        alert('小样申领失败'+msg.Code)
                    }
                }
            })
        },
        //确认申领
        confirmapply:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=confirmapply',
                type: 'post',
                dataType: 'json',
                data:{
                    openid:init.userInfo.Obj.openid,
                    ba_code:init.baCode,
                    sampleId:init.userInfo.getSampleId,
                },
                success: function (msg) {
                    if(msg.Result){
                        if(msg.Code=='1000'){
                            init.mySwiper.slideTo(4,0,false);
                        }else{
                            alert(msg.CodeMsg)
                        }
                    }else{
                        alert(msg.CodeMsg)
                    }
                }
            })
        },
        addbascore:function(){
            $.ajax({
                url: domain+'interface/SampleApplyService.ashx?action=addbascore',
                type: 'post',
                dataType: 'json',
                data:{
                    openid:init.userInfo.Obj.openid,
                    ba_code:init.baCode,
                    sampleId:init.userInfo.getSampleId,
                    score:init.score,
                    comment:encodeURIComponent(init.comment)
                },
                success: function (msg) {
                    if(msg.Result){
                        init.mySwiper.slideTo(5,0,false);
                    }else{
                        alert(msg.CodeMsg)
                    }
                }
            })
        },

    }
    //入口函数
    init.getOauth.oauth();
    //测试注释
    // $('.shu').css('opacity',1);init.getOauth.getSampleQuantity();
})

function updateSwiper(swiper) {
    $(swiper.slides[swiper.previousIndex]).find('.animated').not('.f-not').addClass('js-an');
    $(swiper.slides[swiper.activeIndex]).find('.animated').not('.f-not,.swiper-slide:not(.swiper-slide-active) .animated').removeClass('js-an');
}
//fix: 在ios微信下双击右侧边缘会上移
(function () {
    var agent = navigator.userAgent.toLowerCase(); //检测是否是ios
    var iLastTouch = null; //缓存上一次tap的时间
    if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
        document.body.addEventListener('touchend', function (event) {
            var iNow = new Date().getTime();
            iLastTouch = iLastTouch || iNow + 1 /** 第一次时将iLastTouch设为当前时间+1 */;
            var delta = iNow - iLastTouch;
            if (delta < 500 && delta > 0) {
                event.preventDefault();
                return false;
            }
            iLastTouch = iNow;
        }, false);
    }
})();

function getQueryParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
      results = regex.exec(location.search);
  return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
function checkName(name){
  var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
  if(!reg.test(name)){
    alert('名字格式不正确')
    return false;
  }else{
    return true;
  }
}
function checkNum(phoneNum) {
  if (!/^1[0-9][0-9]\d{8}$/.test(phoneNum)) {
    alert('请输入正确的电话号码')
    return false;
  } else {
    return true;
  }
}
//--swiper实例
init.mySwiper = new Swiper('#swiper0', {
  initialSlide: 0,
  speed: 1,
  followFinger: false,
  resistanceRatio: 0,
  noSwiping: true,
  noSwipingClass: 'stop-swiping',
  touchMoveStopPropagation:true,
  onInit: function onInit(swiper) {
  }
});

//----IScroll实例
$(function(){
    init.scrollPro = new IScroll('#scrollProDate', {
        mouseWheel: true,
        scrollbars: 'custom',
        tap:'scrollTap',
    });
    
    scrollRefresh();
})
$(window).resize(function() {
    scrollRefresh();
});
function scrollRefresh(){
    init.scrollPro.refresh();
}
//--------
//显示小样列表
function appendSample(){
    var j=1;
    for(var i=0;i<init.sampleQuantity.length;i++){
        if(init.sampleQuantity[i].stock!='0'){
            
            $('#scrollProDate .scroll').append('<div class=\'rel page1Img sp' + init.sampleQuantity[i].Id + '\'><i></i><div class=\'rel sampleBox\'><div class=\'rel sampleImg\'><img src=\'images/shu/page1/'
                + (i+1) + '.png\'></div><div class=\'rel sampleDec\'><span class=\'abs sampleId\'>'
                + j + '</span><ul><li class=\'sampleName\'>'
                + init.sampleQuantity[i].sampleName + '</li><li class=\'sampleCapacity\'>'
                + init.sampleQuantity[i].samplecapacity + '</li><li class=\'sampleDes\'>'
                + init.sampleQuantity[i].sampleDes + '</li><li class=\'sampleStock js-none\'>'
                + init.sampleQuantity[i].stock + '</li></ul></div></div></div>');
                j++;
        }
    }
    if(j==1){
        $('#scrollProDate .scroll').append('<div class=\'rel page1Img\'><div class=\'rel sampleBox\'><div class=\'rel sampleDec\' style=\'font-size:0.3rem;\'>' + 
        '本月小样已申领完' + '</div></div></div>');
    }
}
//选择小样
init.sampleLength=0;
$('.slide0').on('scrollTap','.page1Img',function(){
    var a=$(this);
    if(a.hasClass('active')){
        a.removeClass('active');
        init.sampleLength--;
    }else{
        a.addClass('active');
        init.sampleLength++;
    }
})
//同意
$('.slide0').find('.tips').on('click',function(){
    $('span').hasClass('active')?$('span').removeClass('active'):$('span').addClass('active');
})
//确定小样
$('.slide0').find('.apply').on('click',function(){
    
    if(init.sampleLength==1||init.sampleLength==2){
        if($('.slide0 .tips').find('span').hasClass('active')){
            if(!init.userInfo.sampleId){
                init.userInfo.sampleId=getSampleId();
                // init.getOauth.getshoppes();
                $('.fcBoxBg2').show();
                init.getOauth.checkmemberbind();
                trackEvent('apply');
            }
        }else{
            $('.ruleCtx').show();
            $('.fcBoxBg').fadeIn();
        }
    }else{
        $('.wrong').show();
        $('.fcBoxBg').fadeIn();
    }
})
//获取小样id
function getSampleId(){
    var sample='';
    var n=$('.slide0 .page1Img.active').length;
    for(var i=0;i<n;i++){
        sample?sample=sample+'|'+$('.slide0 .page1Img.active')[i].getElementsByTagName('span')[0].innerHTML
            :sample=$('.slide0 .page1Img.active')[i].getElementsByTagName('span')[0].innerHTML
    }
    return sample;
}
//切换城市-并居中
$('.city_sec').change(function(){
    init.shopInfoCity=$(this).val();

    $('.city_area span').html(init.shopInfoCity);

    if(init.shopInfoCity.length==2){
        $(this).css('paddingLeft','42%');
    }
    if(init.shopInfoCity != '城市'){
        $('.store_sec').empty();
        $('.store_sec').append('<option value=\'专柜\'>请选择专柜</option>');
        for (var i = 0; i < init.shops.length; i++) {
            if(init.shops[i].city==init.shopInfoCity){
                for(var j=0;j< init.shops[i].shop.length;j++){
                    $('.store_sec').append('<option value=\'' + init.shops[i].shop[j].name + '\'>' + init.shops[i].shop[j].name + '</option>');
                }
            }
        }
    }
})
//切换店铺-并居中
$('.store_sec').change(function(){
    init.shopInfoStore=$(this).val();

    $('.store_area span').html(init.shopInfoStore);

    if(init.shopInfoStore!='请选择专柜'){
        for (var i = 0; i < init.shops.length; i++) {
            if(init.shops[i].city==init.shopInfoCity){
                for(var j=0;j< init.shops[i].shop.length;j++){
                    if(init.shops[i].shop[j].name==init.shopInfoStore){
                        init.userInfo.shopId=init.shops[i].shop[j].id;
                    }
                }
            }
        }
    }
})
//申领
$('.slide1 .btnBox').find('.apply').on('click',function(){
    waitTime();
    init.userInfo.Obj.nickname=$('.nameVal').val();
    init.userInfo.Obj.usertel=$('.phoneVal').val();
    if($('.city_sec').val()!='城市'){
        if($('.store_sec').val()!='专柜'){
            if(checkName(init.userInfo.Obj.nickname)){
                if(checkNum(init.userInfo.Obj.usertel)){
                    init.getOauth.sampleApply();
                }
            }
        }else{
            alert('请选择专柜')
        }
    }else{
        alert('请选择城市')
    }
})
//返回上一页
$('.slide1 .btnBox').find('.return').on('click',function(){
    init.userInfo.sampleId='';
    init.mySwiper.slideTo(0,0,false);
})

//ba进入
$('.slide3 .apply').on('click',function(){
    init.baCode=$('.codeVal').val();
    init.getOauth.confirmapply();
    trackEvent('confirm');
})
//打分
$('.slide4 .star').find('li').on('click',function(){
    var chsLi=$(this);
    chsLi.addClass('active').prevAll('li').addClass('active');
    chsLi.nextAll('li').removeClass('active');
})
//提交
$('.slide4 .apply').on('click',function(){
    waitTime();
    init.score=$('.slide4 .star').find('li.active').length;
    init.comment=$('.textIdea textarea').val();
    init.getOauth.addbascore();
    trackEvent('submit')
})

//浮层
$('.fcBoxBg').on('click',function(){
    $(this).fadeOut();
    $('.fcCd').fadeOut();
})

//防止连续点击
var tTime;
function waitTime() {
  if (tTime == null){tTime = new Date().getTime();
  }else{
    var t2 = new Date().getTime();
    if(t2 - tTime < 60000){tTime = t2;return;
    }else{tTime = t2;}
  }
}
//tracking
function trackEvent(name) {
    gtag('event', name, {
      'event_category': 'SAMPLING2018',
      'event_label': 'click'
    });
}

