'use strict';
function trackEvent(name) {
	ga('send', 'event', 'IPSA_JPQ', name, 'click');
}
function trackPageview(screenIndex) {
	ga('send', 'pageview', screenIndex);
	//console.log("pv="+screenIndex);
}

$(function() {
	$('[data-track]').on('touchend',function(e) {
		var tid = $(this).attr('data-track');
		trackEvent(tid);
	})
});

